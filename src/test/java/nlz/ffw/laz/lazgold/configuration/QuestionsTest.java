package nlz.ffw.laz.lazgold.configuration;

import nlz.ffw.laz.lazgold.model.GFQuestion;
import nlz.ffw.laz.lazgold.model.ManschaftQuestion;
import nlz.ffw.laz.lazgold.model.MaschinistQuestion;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class QuestionsTest {

    @Autowired
    GFQuestion gfQuestion;

    @Autowired
    MaschinistQuestion maschinistQuestion;

    @Autowired
    ManschaftQuestion manschaftQuestionM;

    @Test
    public void gfQuestionsTest() {
        assertTrue(gfQuestion.getQuestions().get(0).getAnswers().size() == 4);
        assertTrue(gfQuestion.getQuestions().size() == 60);
        assertTrue(gfQuestion.solutions.size() == 60);

        assertTrue(maschinistQuestion.getQuestions().size() == 60);
        assertTrue(maschinistQuestion.getSolutions().size() == 60);

        assertTrue(manschaftQuestionM.getQuestions().size() == 90);
        assertTrue(manschaftQuestionM.getSolutions().size() == 90);
    }

    @Test
    public void testCorrectanswer () {
        gfQuestion.getQuestions().get(0).getAnswers();
        String[] answer = {"a,c"};
        System.out.println(gfQuestion.getSolutions().get(0));
        var parseSolutions = new ArrayList<String>();
        Arrays.asList(gfQuestion.solutions.get(0).split(" ")).forEach(value -> {
            parseSolutions.add(value.substring(0,1));
        });
        System.out.println("Das ist geparste: ");
        for (String s : parseSolutions) {
            System.out.println(s);
        }
    }
}