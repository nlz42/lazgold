package nlz.ffw.laz.lazgold.web;

import nlz.ffw.laz.lazgold.application.Category;
import nlz.ffw.laz.lazgold.application.QuestionService;
import nlz.ffw.laz.lazgold.model.QuestionFormDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class WebController {

    @Autowired
    QuestionService questionService;

    @GetMapping (value = {"/"})
    public String index() {
        return "home";
    }

    @GetMapping(value = "questions/{category}")
    public String getRandom10QuestionFromCategory(
            @PathVariable String category,
            @RequestParam(required = false) Boolean allQuestions,
            Model model) {

        var cat = Category.from(category);
        if (allQuestions != null && allQuestions) {
            model.addAttribute("questions", questionService.getAllQuestionFromCategroy(cat));
        } else {
            model.addAttribute("questions", questionService.getRandom10QuestionFromCategory(cat));
        }
        return "question";
    }

    @PostMapping(value = "questions/{category}")
    public String analyseQuestion(
            @PathVariable String category,
            @ModelAttribute QuestionFormDTO questions, Model model) {

        var result = questionService.solveQuestions(Category.from(category), questions);

        model.addAttribute("result", result);
        return "result";

    }
}
