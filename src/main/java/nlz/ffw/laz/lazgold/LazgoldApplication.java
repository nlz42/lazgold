package nlz.ffw.laz.lazgold;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazgoldApplication {

	public static void main(String[] args) {

		SpringApplication.run(LazgoldApplication.class, args);
	}

}
