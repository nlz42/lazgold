package nlz.ffw.laz.lazgold.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ContentSecurityPolicyConfig {

    @ModelAttribute
    public void cookieHintSecurity(HttpServletResponse response, HttpServletRequest request) {
        log.info("check if csp is needen");
        if(request.getCookies() != null && request.getCookies().length > 0){
            Map<String, String> cookies = Arrays.stream(request.getCookies())
                    .collect(Collectors.toMap(Cookie::getName, Cookie::getValue));
            if (!Boolean.parseBoolean(cookies.getOrDefault("dsgvo", "false"))) {
                log.info("set csp header to avoid 3rd party calls");
                response.addHeader("content-security-policy", "default-src 'self' 'unsafe-inline'; img-src 'self' data:;");
            }
            return;
        }
        log.info("Don't know, to be sure we just set it :-)");
        response.addHeader("content-security-policy", "default-src 'self' 'unsafe-inline'; img-src 'self' data:;");
    }

}
