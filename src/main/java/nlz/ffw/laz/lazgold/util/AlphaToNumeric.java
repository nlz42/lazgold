package nlz.ffw.laz.lazgold.util;

import java.util.HashMap;
import java.util.Map;

public class AlphaToNumeric {

    private static Map<String, Integer> alphaToNumeric = new HashMap<>();

    static {
        alphaToNumeric.put("a)", 0);
        alphaToNumeric.put("b)", 1);
        alphaToNumeric.put("c)", 2);
        alphaToNumeric.put("d)", 3);
        alphaToNumeric.put("e)", 4);
        alphaToNumeric.put("f)", 5);
        alphaToNumeric.put("g)", 6);
        alphaToNumeric.put("h)", 7);
        alphaToNumeric.put("i)", 8);
        alphaToNumeric.put("j)", 9);
        alphaToNumeric.put("k)", 10);
        alphaToNumeric.put("l)", 11);
        alphaToNumeric.put("m)", 12);
        alphaToNumeric.put("n)", 13);
    }

    public static Integer convertAlphaToNumeric(String alpha) {
        return alphaToNumeric.get(alpha);
    }
}
