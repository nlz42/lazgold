package nlz.ffw.laz.lazgold.application;

import nlz.ffw.laz.lazgold.model.*;
import nlz.ffw.laz.lazgold.util.AlphaToNumeric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    @Autowired
    GFQuestion gfQuestion;

    @Autowired
    MaschinistQuestion maschinistQuestion;

    @Autowired
    ManschaftQuestion manschaftQuestion;

    public QuestionFormDTO getAllQuestionFromCategroy (Category category) {
        ArrayList<Question> questionRepsonse = new ArrayList<Question>();
        switch (category) {
            case GF:
                for (int i = 0; i < 60; i++) {
                    var value = gfQuestion.getQuestions().get(0 + i);
                    questionRepsonse.add(value);
                }
                break;
            case Maschinist:
                for (int i = 0; i < 60; i++) {
                    var value = maschinistQuestion.getQuestions().get(0 + i);
                    questionRepsonse.add(value);
                }
                break;
            default:
                for (int i = 0; i < 90; i++) {
                    var value = manschaftQuestion.getQuestions().get(0 + i);
                    questionRepsonse.add(value);
                }
                break;
        }
        return new QuestionFormDTO(0, questionRepsonse);

    }

    public QuestionFormDTO getRandom10QuestionFromCategory(Category category) {
        var startindex = randomStartPoint(category);
        var questionRepsonse = new ArrayList<Question>();

        switch (category) {
            case GF:
                for (int i = 0; i < 10; i++) {
                    var value = gfQuestion.getQuestions().get(startindex + i);
                    questionRepsonse.add(value);
                }
                break;
            case Maschinist:
                for (int i = 0; i < 10; i++) {
                    var value = maschinistQuestion.getQuestions().get(startindex + i);
                    questionRepsonse.add(value);
                }
                break;
            default:
                for (int i = 0; i < 10; i++) {
                    var value = manschaftQuestion.getQuestions().get(startindex + i);
                    questionRepsonse.add(value);
                }
                break;
        }
        return new QuestionFormDTO(startindex, questionRepsonse);
    }

    public QuestionResultForm solveQuestions(Category category, QuestionFormDTO questions) {
        var result = new QuestionResultForm();
        switch (category) {
            case GF:
                var gfSolutions = convertSolutionsToIntegerList(gfQuestion.getSolutions());
                for (int i = 0; i < questions.getQuestions().size(); i++) {
                    var answer = checkAnswer(gfSolutions, questions.getQuestions().get(i).getAnswers(), i + questions.getStartQuestionIndex());
                    if (!answer) {
                        var questIndex = i + questions.getStartQuestionIndex();
                        var answers = new ArrayList<String>();
                        for (int answerIndex : gfSolutions.get(questIndex)) {
                            answers.add(gfQuestion.getQuestions().get(questIndex).getAnswers().get(answerIndex));
                        }
                        var questionWithAnswer = new QuestionWithAnswers(
                                questions.getStartQuestionIndex(),
                                String.valueOf(questIndex+1 +" ")+questions.getQuestions().get(i).getQuestion(),
                                answers);
                        result.getQuestion().add(questionWithAnswer);
                    }
                }
                break;
            case Maschinist:
                var maschinistSolutions = convertSolutionsToIntegerList(maschinistQuestion.getSolutions());
                for (int i = 0; i < questions.getQuestions().size(); i++) {
                    var answer = checkAnswer(maschinistSolutions, questions.getQuestions().get(i).getAnswers(), i + questions.getStartQuestionIndex());
                    if (!answer) {
                        var questIndex = i + questions.getStartQuestionIndex();
                        var answers = new ArrayList<String>();
                        for (int answerIndex : maschinistSolutions.get(questIndex)) {
                            answers.add(maschinistQuestion.getQuestions().get(questIndex).getAnswers().get(answerIndex));
                        }
                        var questionWithAnswer = new QuestionWithAnswers(
                                questions.getStartQuestionIndex(),
                                String.valueOf(questIndex+1 +" ")+questions.getQuestions().get(i).getQuestion(),
                                answers);
                        result.getQuestion().add(questionWithAnswer);
                    }
                }
                break;
            default:
                var manschaftSolutions = convertSolutionsToIntegerList(manschaftQuestion.getSolutions());
                for (int i = 0; i < questions.getQuestions().size(); i++) {
                    var answer = checkAnswer(manschaftSolutions, questions.getQuestions().get(i).getAnswers(), i + questions.getStartQuestionIndex());
                    if (!answer) {
                        var questIndex = i + questions.getStartQuestionIndex();
                        var answers = new ArrayList<String>();
                        for (int answerIndex : manschaftSolutions.get(questIndex)) {
                            answers.add(manschaftQuestion.getQuestions().get(questIndex).getAnswers().get(answerIndex));
                        }
                        var questionWithAnswer = new QuestionWithAnswers(
                                questions.getStartQuestionIndex(),
                                String.valueOf(questIndex+1 +" ")+questions.getQuestions().get(i).getQuestion(),
                                answers);
                        result.getQuestion().add(questionWithAnswer);
                    }
                }
                break;
        }

        var amountOfQuestions = Double.valueOf(questions.getQuestions().size());
        var amountOfWrongAnswers = Double.valueOf(result.getQuestion().size());

        var wrongsAnswers = ((amountOfWrongAnswers / amountOfQuestions) * 100.0);

        if (wrongsAnswers < 25.0) {
            result.setMessage("Herzlichen Glückwunsch, du hast bestanden. Richtige Antworten = " + (100 - wrongsAnswers) + "%");
        } else {
            result.setMessage("Zu viele Falsche Antworten (75% muss richitg sein!). Richtige Antworten = " + (100 - wrongsAnswers) + "%");
        }
        return result;
    }

    private boolean checkAnswer(ArrayList<ArrayList<Integer>> solutions, List<String> answers, int questIndex) {

        List<String> answersWithoutNull = answers.parallelStream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (solutions.get(questIndex).size() != answersWithoutNull.size()) {
            return false;
        }

        int index = 0;
        for (String answer : answersWithoutNull) {
            if (!Integer.valueOf(answer).equals(solutions.get(questIndex).get(index))) {
                return false;
            }
            index++;
        }
        return true;
    }


    private int randomStartPoint(Category cat) {

        if (cat.equals(Category.Manschaft)) {
            return (int) (Math.random() * 80);
        } else {
            return (int) (Math.random() * 50);
        }
    }

    private ArrayList<ArrayList<Integer>> convertSolutionsToIntegerList(List<String> solutions) {
        // convert solutions in a good format
        var convertedSolution = new ArrayList<ArrayList<Integer>>();
        int listIndex = 0;
        for (String solution : solutions) {
            var s = solution.split("[ ,]");
            convertedSolution.add(new ArrayList<>());
            for (String entry : s) {
                var numericSolution = AlphaToNumeric.convertAlphaToNumeric(entry);
                if (numericSolution != null) {
                    convertedSolution.get(listIndex).add(numericSolution);
                }
            }
            listIndex++;
        }
        return convertedSolution;
    }
}
