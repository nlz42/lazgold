package nlz.ffw.laz.lazgold.application;

import java.util.Arrays;

public enum Category {

    GF("gf"), Maschinist("maschinist"), Manschaft("manschaft");

    String shortCatgeory;

    Category(String cat) {
        this.shortCatgeory = cat;
    }

    public static Category from(String category) {

        return Arrays.stream(Category.values()).filter(v -> v.shortCatgeory.equalsIgnoreCase(category)).findFirst().get();
    }
}
