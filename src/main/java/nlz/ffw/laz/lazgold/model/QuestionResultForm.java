package nlz.ffw.laz.lazgold.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResultForm {

    private String message;

    private List<QuestionWithAnswers> question = new ArrayList<>();



}
