package nlz.ffw.laz.lazgold.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class QuestionFormDTO {

    private Integer startQuestionIndex;
    private List<Question> questions;
}

