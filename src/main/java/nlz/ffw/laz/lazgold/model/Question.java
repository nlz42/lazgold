package nlz.ffw.laz.lazgold.model;

import lombok.Data;

import java.util.List;

@Data
public class Question {

    private String question;
    private List<String> answers;
}
