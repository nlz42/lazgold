package nlz.ffw.laz.lazgold.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class QuestionWithAnswers {

    private Integer startIndex;

    private String question;

    private List<String> answers;
}
