package nlz.ffw.laz.lazgold.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "gf")
@Data
public class GFQuestion {
    List<Question> questions;
    public List<String> solutions;
}
