package nlz.ffw.laz.lazgold.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "maschinist")
@Data
public class MaschinistQuestion {
    List<Question> questions;
    List<String> solutions;

}
