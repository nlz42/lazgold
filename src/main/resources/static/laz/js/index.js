document.addEventListener("DOMContentLoaded", function(){
	//add pop up here!
	checkCookieConsent();
});

function checkCookieConsent() {

	let hasDsgvoCookie = getCookie("dsgvo");
	if( hasDsgvoCookie === "") {
		//nothing selected -> show hint!
		$('#dsgvomodal').modal('show');
	}
}

function setUserSelectionCookie(accept) {
	console.log("set cookie")
	document.cookie = "dsgvo="+accept+";" + "expires="+ new Date(new Date().getTime()+60*60*1000*24).toGMTString();
	location.reload();
}

function getCookie(cname) {
	let name = cname + "=";
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for(let i = 0; i <ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}